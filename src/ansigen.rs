use crate::color::{Color3, Color4};
use ansi_term::{ANSIString, ANSIStrings, Color::RGB as AnsiRGB, Style};
use image::RgbaImage;
use std::convert::TryInto;
use std::io::{self, Write};

/// Generates an ANSI sequence that contains two pixels.
///
/// This uses the unicode 'UPPER HALF BLOCK' (U+2580) character with
/// set background and foreground color. The foreground color will only apply
/// to the top half, while the background color will only be visible in the bottom half.
pub fn create_vertical_pixel_pair(top: Color4, bottom: Color4) -> ANSIString<'static> {
    let top = top.blend(Color3::black());
    let bottom = bottom.blend(Color3::black());
    Style::new()
        .on(AnsiRGB(bottom.0, bottom.1, bottom.2))
        .fg(AnsiRGB(top.0, top.1, top.2))
        .paint("▀")
}

/// Generates ANSI text from an RgbaImage, and writes it to ``output``.
/// 
/// All alpha pixels are blended with black.
pub fn generate_ansi_from_image(image: &RgbaImage, output: &mut impl Write) -> io::Result<()> {
    let mut rows = image.rows();
    let mut ansi_buf: Vec<ANSIString<'static>> = Vec::with_capacity(
        ((image.width() + 1) * (image.height() / 2))
            .try_into()
            .unwrap(),
    );
    while let Some(row) = rows.next() {
        let row = row.map(|pixel| Color4::from(pixel.0));
        if let Some(next_row) = rows.next() {
            for pair in row.zip(next_row.map(|pixel| Color4::from(pixel.0))) {
                let text = create_vertical_pixel_pair(pair.0, pair.1);
                ansi_buf.push(text);
            }
        } else {
            for pixel in row {
                let text = create_vertical_pixel_pair(pixel, Color4::transparent());
                ansi_buf.push(text);
            }
        }
        ansi_buf.push(Style::default().paint("\n"));
    }
    output.write_all(ANSIStrings(&ansi_buf).to_string().as_bytes())?;
    Ok(())
}

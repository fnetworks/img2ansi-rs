use clap::{App, Arg};
use image::{self, imageops::FilterType};
use img2ansi::ansigen;
use std::io::{stdout, Write};

#[cfg(windows)]
fn check_term_support(warn: bool) {
    if let Err(code) = ansi_term::enable_ansi_support() {
        if warn {
            eprintln!(
                "Warning: your terminal doesn't support ANSI codes (code {:#x}).",
                code
            );
            eprintln!("If you are printing to a file, you can ignore this message.");
        }
    }
}

#[cfg(not(windows))]
fn check_term_support(_warn: bool) {
    // Assume terminal supports ansi
}

const PIXEL_SIZE: (u32, u32) = (8, 8);

fn main() {
    let matches = App::new("img2ansi")
        .about("Converts an image to ANSI escape codes that can be displayed in terminals")
        .usage("img2ansi <INPUT> [OUTPUT]") // Clap wants to show filter option in usage, which is IMO not necessary
        .arg(
            Arg::with_name("INPUT")
                .required(true)
                .help("The input image")
                .index(1),
        )
        .arg(
            Arg::with_name("OUTPUT")
                .default_value("-")
                .help("The output file. Use '-' for stdout")
                .allow_hyphen_values(true)
                .index(2),
        )
        .arg(
            Arg::with_name("filter")
                .takes_value(true)
                .long("filter")
                .short("f")
                .possible_values(&["catmull_rom", "gaussian", "lanczos3", "nearest", "triangle"])
                .default_value("nearest")
                .help("Sets the resize filter to use"),
        )
        .get_matches();

    let path = matches.value_of("INPUT").unwrap();
    let output_path = matches.value_of("OUTPUT").unwrap_or("-");
    let filter = match matches.value_of("filter").unwrap_or("nearest") {
        "catmull_rom" => FilterType::CatmullRom,
        "gaussian" => FilterType::Gaussian,
        "lanczos3" => FilterType::Lanczos3,
        "nearest" => FilterType::Nearest,
        "triangle" => FilterType::Triangle,
        _ => panic!("Illegal filter type"),
    };

    let img = image::open(path).expect("Could not open image");
    let rgb_image = img.to_rgb();
    let img = img.resize(
        rgb_image.width() / PIXEL_SIZE.0,
        rgb_image.height() / PIXEL_SIZE.1,
        filter,
    );
    let rgb_image = img.to_rgba();

    check_term_support(output_path == "-");

    let mut output: Box<dyn Write> = if output_path == "-" {
        Box::new(stdout())
    } else {
        Box::new(std::fs::File::create(output_path).unwrap())
    };
    ansigen::generate_ansi_from_image(&rgb_image, &mut output).expect("Failed to write to output");
}

/// A three-component color (red, green, blue)
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Color3(pub u8, pub u8, pub u8);

impl Color3 {
    /// Creates a new Color3 from the specified RGB values
    pub fn new(r: u8, g: u8, b: u8) -> Self {
        Color3(r, g, b)
    }

    /// The color black (0, 0, 0)
    pub fn black() -> Self {
        Self(0, 0, 0)
    }

    /// The color white (255, 255, 255)
    pub fn white() -> Self {
        Self(255, 255, 255)
    }

    /// Returns the red component of the color
    pub fn red(self) -> u8 {
        self.0
    }

    /// Returns the green component of the color
    pub fn green(self) -> u8 {
        self.1
    }

    /// Returns the blue component of the color
    pub fn blue(self) -> u8 {
        self.2
    }
}
impl From<[u8; 3]> for Color3 {
    fn from(t: [u8; 3]) -> Self {
        Color3(t[0], t[1], t[2])
    }
}

impl From<Color4> for Color3 {
    fn from(t: Color4) -> Self {
        Self(t.0, t.1, t.2)
    }
}

impl std::ops::Mul<f32> for Color3 {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self {
        let r: f32 = self.0.into();
        let g: f32 = self.1.into();
        let b: f32 = self.2.into();
        Self(
            (r * rhs).round().max(0.0).min(255.0) as u8,
            (g * rhs).round().max(0.0).min(255.0) as u8,
            (b * rhs).round().max(0.0).min(255.0) as u8,
        )
    }
}

impl std::ops::Add<Color3> for Color3 {
    type Output = Self;

    fn add(self, rhs: Color3) -> Self {
        Self(
            self.0.saturating_add(rhs.0),
            self.1.saturating_add(rhs.1),
            self.2.saturating_add(rhs.2),
        )
    }
}

/// A four-component color (red, green, blue, alpha)
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Color4(pub u8, pub u8, pub u8, pub u8);

impl Color4 {
    /// Creates a new Color4 from the specified RGBA values
    pub fn new(r: u8, g: u8, b: u8, a: u8) -> Self {
        Self(r, g, b, a)
    }

    /// Returns a fully transparent color (0, 0, 0, 0)
    pub fn transparent() -> Self {
        Self(0, 0, 0, 0)
    }

    /// Returns the color black (0, 0, 0, 255)
    pub fn black() -> Self {
        Self(0, 0, 0, 255)
    }

    /// Returns the color white (255, 255, 255, 255)
    pub fn white() -> Self {
        Self(255, 255, 255, 255)
    }

    /// Creates a Color4 from a Color3 and an alpha value
    pub fn from_color3(color3: Color3, alpha: u8) -> Self {
        Self(color3.0, color3.1, color3.2, alpha)
    }

    /// Blends this color and a background color together
    pub fn blend(self, background: Color3) -> Color3 {
        alpha_blend(self.into(), background, self.alpha())
    }

    /// Returns the red component of the color
    pub fn red(self) -> u8 {
        self.0
    }

    /// Returns the green component of the color
    pub fn green(self) -> u8 {
        self.1
    }

    /// Returns the blue component of the color
    pub fn blue(self) -> u8 {
        self.2
    }

    /// Returns the alpha component of the color
    pub fn alpha(self) -> u8 {
        self.3
    }
}

impl From<[u8; 4]> for Color4 {
    fn from(t: [u8; 4]) -> Self {
        Color4(t[0], t[1], t[2], t[3])
    }
}

impl From<Color3> for Color4 {
    fn from(t: Color3) -> Self {
        Self(t.0, t.1, t.2, 255)
    }
}

impl std::ops::Mul<f32> for Color4 {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self {
        let r: f32 = self.0.into();
        let g: f32 = self.1.into();
        let b: f32 = self.2.into();
        let a: f32 = self.3.into();
        Self(
            (r * rhs).round().max(0.0).min(255.0) as u8,
            (g * rhs).round().max(0.0).min(255.0) as u8,
            (b * rhs).round().max(0.0).min(255.0) as u8,
            (a * rhs).round().max(0.0).min(255.0) as u8,
        )
    }
}

impl std::ops::Add<Color4> for Color4 {
    type Output = Self;

    fn add(self, rhs: Color4) -> Self {
        Self(
            self.0.saturating_add(rhs.0),
            self.1.saturating_add(rhs.1),
            self.2.saturating_add(rhs.2),
            self.3.saturating_add(rhs.3),
        )
    }
}

/// Blends two colors by the specified amount
pub fn alpha_blend(foreground: Color3, background: Color3, amount: u8) -> Color3 {
    let amount = amount as f32 / 255f32;
    foreground * amount + background * (1.0 - amount)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_alpha_blend() {
        assert_eq!(
            alpha_blend(Color3::new(10, 20, 30), Color3::new(0, 0, 0), 255),
            Color3::new(10, 20, 30)
        );
        assert_eq!(
            alpha_blend(Color3::new(10, 20, 30), Color3::new(0, 0, 0), 0),
            Color3::new(0, 0, 0)
        );
    }
}

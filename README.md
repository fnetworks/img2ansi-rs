# img2ansi-rs

Converts an image to ANSI escape codes

Support for Windows 10 is untested, but it should work (at least on newer versions).

## Usage

```sh
cargo run <filename>
```

## Disclaimer

This project is still a work in progress.
See [TODO](TODO) for things that are missing or not working properly.